<?php
namespace Empu\Region\Data;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'modules/region/seeders/region.sql';
        DB::connection('region')->unprepared(file_get_contents($path));
        $this->command->info('RegionSeeder\'s tables seeded!');
    }
}
