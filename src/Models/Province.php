<?php
/*** Bismillahirrahmanirrahim ***/
namespace Empu\Region\Models;

use Pusaka\Geni\Eloquent\Model;

class Province extends Model
{
    use ModifiedAttribute;

    public $connection = 'region';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'provinces';

    protected $fillable = [];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public function regencies()
    {
        return $this->hasMany(Regency::class);
    }
}
