<?php
/*** Bismillahirrahmanirrahim ***/
namespace Empu\Region\Models;

use Pusaka\Geni\Eloquent\Model;

class Regency extends Model
{
    use ModifiedAttribute;

    public $connection = 'region';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'regencies';

    protected $fillable = [];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
}
