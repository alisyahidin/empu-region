<?php
/*** Bismillahirrahmanirrahim ***/
namespace Empu\Region\Models;

trait ModifiedAttribute
{
    public function getNameAttribute()
    {
        return ucwords(strtolower($this->attributes['name']));
    }
}